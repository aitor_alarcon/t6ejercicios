import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T6Ejercicio1App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro la constante PI
		final double PI = 3.1416;
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pregunto de que figura quiere calcular el area
		System.out.println("De que figura quieres calcular el area? \n 1)Circulo, 2)Triangulo, 3)Cuadrado");
		// Guardamos la eleccion
		int eleccion = teclado.nextInt();
		
		// Creo un switch
		switch (eleccion) {
		// En el case 1 hacemos el circulo
		case 1:
			// Pedimos el radio del circulo
			System.out.println("Dime el radio del circulo por favor");
			double radio = teclado.nextDouble();
			
			// Llamamos a la funcion areaCirculo
			System.out.println("El area del circulo es = " + areaCirculo(radio, PI));
			
			break;
			
		// En el case 2 hacemos el triangulo
		case 2:
			// Pido la base
			System.out.println("Dime la base del triangulo por favor");
			double base = teclado.nextDouble();
			// Pido la altura
			System.out.println("Dime la altura del triangulo por favor");
			double altura = teclado.nextDouble();
			
			// Llamamos a la funcion areaTriangulo
			System.out.println("El area del tringulo es = " + areaTriangulo(base, altura));
			
			break;
			
		// En el case 3 hacemos el cuadrado
		case 3:
			// Pido el lado
			System.out.println("Dime lo que mide el lado del cuadrado por favor");
			double lado = teclado.nextDouble();
			
			// LLamo a la funcion areaCuadrado
			System.out.println("El area del cuadrado es = " + areaCuadrado(lado));
			
			break;
			
		default:
			// Como case default le digo que no ha elegido ninguno
			System.out.println("No has elegido ninguno");
			break;
		}
		
	}
	
	// Creo la funcion areaCirculo
	public static double areaCirculo (double radio, double PI) {
		
		// Calculo el area del circulo
		double area = Math.pow(radio, 2)*PI;
		
		// Retornamos el area
		return area;
	}
	
	// Creo la funcion areaTriangulo
	public static double areaTriangulo (double base, double altura) {
		
		// Calculo el area del triangulo
		double area = (base * altura) / 2;
		
		// Retornamos el area
		return area;
	}
	
	// Creo la funcion areaCuadrado
	public static double areaCuadrado (double lado) {
		
		// Calculo el area del cuadrado
		double area = Math.pow(lado, 2);
		
		// Retornamos el area
		return area;
	}

}
