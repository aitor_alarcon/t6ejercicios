import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T6Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pido un n�mero por teclado
		System.out.println("Introduce un n�mero");
		// Guardo el n�mero
		int num = teclado.nextInt();
		
		// Llamo a la funci�n y muestro el factorial
		System.out.println("Factorial del num " + num + " = " + factorial(num));
		
	}
	
	// Creo la funci�n factorial
	public static int factorial (int num) {
		
		// Creo el contador factorial que ir� almacenando el resultado final
		int factorial = 1;
				
		// Creo el for que calcular� el factorial
		for (int i = num; i > 0; i--) {
			// Calculo y acumulo el resultado con factorial despu�s de cada vuelta de reloj
			factorial = factorial * i;
		}
		
		// Retorno factorial
		return factorial;
	}

}
