import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T6Ejercicio2App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pido cu�ntos n�meros aleatorios quiere crear
		System.out.println("Cuantos n�meros aleatorios quieres crear?");
		// Almaceno la cantidad de n�meros
		int cantNumAleatorios = teclado.nextInt();
		
		// Pido al usuario entre que n�mero quiere generar los aleatorios
		System.out.println("Entre que n�meros quieres que te de los n�meros aleatorios?");
		System.out.println("Numero 1");
		int num1 = teclado.nextInt();
		System.out.println("Numero 2 (Tiene que ser mayor que el anterior)");
		int num2 = teclado.nextInt();
		
		// Creo un bucle for
		for (int cont = 0; cont < cantNumAleatorios; cont++) {
			// Llamamos e imprimimos el m�todo numAleatorio
			System.out.println("N�mero aleatorio " + (cont+1) + " = " + numAleatorio(num1, num2));
		}
	}
	
	// Creamos la funcion numAleatorio
	public static int numAleatorio (int numMenor, int numMayor) {
		
		// Creo el algoritmo que va a dar el n�mero aleatorio
		int numAleatorio = (int) Math.floor(Math.random()*(numMayor-numMenor+1)+numMenor);
		
		// Retornamos el n�mero aleatorio
		return numAleatorio;
	}

}
