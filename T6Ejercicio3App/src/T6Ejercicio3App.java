import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T6Ejercicio3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Le pido que introduzca el n�mero
		System.out.println("Introduce un n�mero");
		// Guardo el num
		int num = teclado.nextInt();
		
		// Dependiendo del resultado del m�todo
		if(numPrimo(num) == true) {
			// imprimo si es primo
			System.out.println("El n�mero " + num + " es primo");
		}
		else {
			// o si no es primo
			System.out.println("El n�mero " + num + " no es primo");
		}
		
		
	}
	
	// Creo el m�todo numPrimo
	public static boolean numPrimo (int num) {
		
		// Creo un contador iniciado a 0
		int cont = 0;
		
		// Creo un bucle for que recorra el numero de veces igual 
		// al n�mero introducido
		for (int i = 1; i <= num; i++) {
			// Si el n�mero introducido entre el contador i 
			// es igual a 0 incremento el contador en 1
			if((num % i) == 0) {
				// Incremento el contador
				cont++;
			}
		}
		
		// si el contador es menor o igual a 2
		if(cont <= 2) {
			// Retorna true
			return true;
		}
		// si el contador es mayor que 2
		else {
			// Retorna false
			return false;
		}
	}

}
