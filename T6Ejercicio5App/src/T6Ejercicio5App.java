import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T6Ejercicio5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pido al usuario que introduzca un n�mero
		System.out.println("Introduce un n�mero por favor");
		double num = teclado.nextInt();
		
		// Llamo al funcion y muestro el n�mero en binario
		System.out.println("El n�mero " + num + " en binario es = " + binario(num));
		
	}
	
	// Creo la funcion binario
	public static String binario(double num) {
		
		// Creo el String que retornar� m�s adelante
		String binario = "";
		
		// Creo un if que si el numero introducido es mayor que 0
		// entre en el
		if(num > 0) {
			// Creo un while que mientras el numero se mayor a 0
			// recorra el bucle
			while(num > 0) {
				// Dentro creo un if que si el num dividido entre 2 el residuo es igual a 0
				if(num % 2 == 0) {
					// Coloque un 0 en el String
					binario = "0" + binario;
				}
				else {
					// Si no es igual a 0 coloque un 1
					binario = "1" + binario;
				}
				
				// Y hago que el n�mero introducido se vaya dividiendo entre 2
				num = (int) num / 2;
			}
		}
		// Si el n�mero que me han introducido es un cero mostrar�
		else if(num == 0) {
			// Que en binario es un 0
			binario = "0";
		}
		// Y si no es positivo muestre
		else {
			// Que no se ha podido convertir
			binario = "No se ha podido convertir el n�mero";
		}
		
		// Retorno binario
		return binario;
	}

}
