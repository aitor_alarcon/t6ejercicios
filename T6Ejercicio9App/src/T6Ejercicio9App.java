import java.util.Scanner;

public class T6Ejercicio9App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro los dos valores y los inicializo
		int valor1 = 0, valor2 = 9;
		
		// Llamo a la funci�n rellenarArray
		rellenarArray(valor1, valor2);
	}
	
	// Creo la funci�n rellenarArray
	public static void rellenarArray(int valor1, int valor2) {
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Le digo que introduzca el n�mero de espacios que tendra el array
		System.out.println("Introduce un numero para el tama�o del array");
		int num =  teclado.nextInt();
		
		// Creo el array
		int array[] = new int[num];
		
		// Relleno el array con valores aleatorios del 0 al 9
		for (int i = 0; i < array.length; i++) {
			int numAleatorio = (int) Math.floor(Math.random()*(valor1-valor2+1)+valor2);
			array[i] = numAleatorio;
		}
		
		// Llamo a la funcion de mostrar array
		mostrarArray(array);
	}
	
	// Creo la funci�n de mostrarArray
	public static void mostrarArray(int[] array) {
		
		// Declaro la variable suma y la inicializo
		int suma = 0;
		
		// Creo un for para que me haga la suma de todos los elementos del array
		for (int n = 0; n < array.length; n++) {
			suma = suma + array[n];
		}
		
		// Creo un for para que me muestre todos los elementos del array
		for (int i = 0; i < array.length; i++) {
			System.out.println("En la posici�n " + i + " hay este valor: " + array[i]);
		}
		
		// Muestro la suma de todos los elementos del array
		System.out.println("La suma de todos los elementos de la array es = " + suma);
	}
	
	

}
