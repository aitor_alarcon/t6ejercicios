import java.util.Scanner;

public class T6Ejercicio12App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaramos el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Le pido al usuario que me introduzca un n�mero
		System.out.println("Introduce un n�mero de 10 para abajo para crear el array");
		int numEspaciosArray = teclado.nextInt();
		
		// Creo el array 
		int arrayRandom[] = new int[numEspaciosArray];
		
		// Creo un if para controlar los espacios del array porque si no me da out of bounds
		if(numEspaciosArray <= 10) {
			rellenarArray(arrayRandom);
		}
		// Si no me tiene que volver a introducir el n�mero
		else {
			System.out.println("Vuelve a introducir el n�mero");
			numEspaciosArray = teclado.nextInt();
		}
		
	}
	
	// Creo el m�todo rellenarArray
	public static void rellenarArray(int array[]) {
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Declaro el rango de numeros del rango
		int num1 = 1, num2 = 300;
		
		// Creo un for que rellene el array que ha definido el usuario 
		// Con n�meros random
		for (int i = 0; i < array.length; i++) {
			int numAleatorio = (int) Math.floor(Math.random()*(num1-num2+1)+num2);
			array[i] = numAleatorio;
		}
		
		// Le pido que introduzca un n�mero para ense�ar cuales otros n�meros
		// Han acabado en el escogido
		System.out.println("Introduce un n�mero para ver cuales de ellos acaban en el que has escogido");
		System.out.println("Y los introducir� en otro array");
		int eleccionNum = teclado.nextInt();
		
		// Controlo que el n�mero introducido es positivo y menor que 10
		do {
			if(eleccionNum < 0 || eleccionNum > 9) {
				System.out.println("Te has equivocado vuelve a poner otro n�mero");
				eleccionNum = teclado.nextInt();
			}
		} while (eleccionNum < 0 || eleccionNum > 9);
		
		// Aviso al usuario de que voy a imprimir los n�meros escogidos
		System.out.println("Estos son los n�meros que se han generado que tienen como �ltimo d�gito"
				+ " el n�mero que has escogido");
		
		// Llamo a la funci�n arrayN�meroEspecifico
		arrayNumeroEspecifico(array, eleccionNum);
		
	}
	
	// Creo el m�todo arrayNumeroEspecifico
	public static void arrayNumeroEspecifico (int array[], int num) {
		
		// Creo el array donde saldran n�meros acabados en el n�mero que 
		// ha escogido el usuario
		int arrayNumeroEspecifico[] = new int[10];
		
		// Creo un for para rellenar el array nuevo con los n�meros espec�ficos
		for (int i = 0; i < array.length; i++) {
			if(array[i]%10 == num) {
				arrayNumeroEspecifico[i] = array[i];
			}
		}
		
		// Y muestro el array
		for (int k = 0; k < arrayNumeroEspecifico.length; k++) {
			System.out.print(arrayNumeroEspecifico[k] + " ");
		}
	}

}
