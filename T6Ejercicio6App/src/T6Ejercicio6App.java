import java.util.Scanner;

public class T6Ejercicio6App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pido el n�mero
		System.out.println("Introduce un n�mero positivo");
		long num =  teclado.nextInt();
		
		// Controlo si el n�mero introducido es positivo
		do {
			if(num < 0) {
				System.out.println("No has introducido un n�mero positivo, vuelve a intentarlo");
				num = teclado.nextInt();
			}
		} while (num < 0);
		
		// Llamo a la funci�n numCifras y muestro
		System.out.println("El n�mero " + num + " tiene " + numCifras(num) + " cifras");
		
	}
	
	// Creo la funci�n numCifras
	public static long numCifras (long num) {
		
		// Creo el String numCifras y paso el num a String
		String numCifras = Long.toString(num);
		
		// Retorno la cantidad de cifras que tiene el numero
		return numCifras.length();
	}

}
