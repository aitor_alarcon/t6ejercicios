import java.util.Scanner;

public class T6Ejercicio8App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Le pido al usuario que introduzca 10 valores
		System.out.println("Introduce 10 valores");
		
		// Llamo a la funci�n rellenarArray();
		rellenarArray();
	}
	
	// Creo el m�todo rellenarArray
	public static void rellenarArray(){
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Declaro el array de 10 posiciones
		int array[] = new int[10];
		
		// Creo el for y hago que introduzca un valor por cada vuelta de reloj
		for (int i = 0; i < array.length; i++) {
			System.out.println("Introduce el valor " + (i+1));
			int valor = teclado.nextInt();
			array[i] = valor;
		}
		
		// Llamo a la funci�n de mostrar el array
		mostrarArray(array);
	}
	
	// Creo la funci�n mostrarArray
	public static void mostrarArray(int array[]) {
		
		// Creo un for para mostrar todos los valores del array
		for (int i = 0; i < array.length; i++) {
			System.out.println("El n�mero en la posici�n " + i + " es = " + array[i]);
		}
	}

}
