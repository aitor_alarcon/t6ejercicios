import java.util.Scanner;

public class T6Ejercicio11App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Llamo a la funci�n crearArray
		crearArray();
		
	}
	
	// Creo el m�todo crearArray
	public static void crearArray(){
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Declaro el rango en el que se generar�n los n�meros aleatorios
		int num1 = 10, num2 = 250;
		
		// Pregunto al usuario que espacio quiere asignar a los arrays
		System.out.println("De cuanto quieres que sean los arrays?");
		int numEspacios = teclado.nextInt();
		
		// Creo el array1
		int array1[] = new int[numEspacios];
		
		// Llamo a la funci�n para rellenar el array
		rellenarArray(array1, num1, num2);
		
		// Creo el array2
		int array2[] = new int[numEspacios];
		
		// Asigno el array2 al array1
		array2 = array1;
		
		// Vuelvo a asignar espacios al array1 
		array1 = new int[numEspacios];
		
		// Vuelvo a llamar a la funci�n rellenarArray
		rellenarArray(array1, num1, num2);
		
		// Llamo a la funci�n rellenarTercerArray
		rellenarTercerArray(array1, array2);
	}
	
	// Creo la funci�n rellenarArray
	public static void rellenarArray(int array[], int num1, int num2) {
		
		// Con un for dentro para poder rellenar los arrays
		for (int i = 0; i < array.length; i++) {
			int numAleatorio = (int) Math.floor(Math.random()*(num1-num2+1)+num2);
			array[i] = numAleatorio;
		}
		
	}
	
	// Creo el m�todo rellenarTercerArray
	public static void rellenarTercerArray(int array1[], int array2[]) {
		
		// Creo el Array3 a partir de la medida del array 1
		int array3[] = new int[array1.length];
		
		// Relleno el array 3 con la multiplicaci�n de cada uno de los espacios
		// de los otros arrays
		for (int i = 0; i < array1.length; i++) {
			array3[i] = array1[i]*array2[i];
		}
		
		// Llamo al m�todo mostrarMatrices para poder mostrar las tres creadas este ejercicio
		mostrarMatrices(array1, array2, array3);
		
	}
	
	// Creo la funcion mostrarMatrices
	public static void mostrarMatrices(int array1[], int array2[], int array3[]) {
		
		// Creo un for por cada uno de los Arrays para poder mostrarlos lo m�s limpios posibles
		for (int i = 0; i < array1.length; i++) {
			System.out.print(array1[i] + " ");
		}
		
		// Y pongo un syso vac�o para separarlos
		System.out.println("");
		
		for (int j = 0; j < array2.length; j++) {
			System.out.print(array2[j] + " ");
		}
		
		System.out.println(" ");
		
		for (int n = 0; n < array3.length; n++) {
			System.out.print(array3[n] + " ");
		}
	}

}
