import java.util.Scanner;

public class T6Ejercicio7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro un Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Le pido al usuario la cantidad de euros
		System.out.println("Por favor introduce una cantidad de euros");
		double euros = teclado.nextDouble();
		
		// Y en que moneda los quiere transformar
		System.out.println("Introduce la moneda a la que quieres pasar los euros");
		System.out.println("libras, dolares, yenes");
		String moneda = teclado.next();
		
		// Llamo a la funcion cambioDivisa
		cambioDivisa(euros, moneda);
	}
	
	// Creo el m�todo cambioDivisa
	public static void cambioDivisa(double euros, String moneda) {
		
		// Creo un switch que depender� de la elecci�n de moneda que haya hecho el usuario
		switch (moneda) {
		// Creo el case libras
		case "libras":
			// Calculo el importe de euros en libras
			double libras = euros*0.86;
			
			// Muestro el resultado
			System.out.println("Los " + euros + "� " + " son equivalentes a " + libras + " libras");
			break;
		
		// Creo el case dolares
		case "dolares":
			// Calculo el importe de euros en dolares
			double dolares = euros*1.28611;
			
			// Muestro el resultado
			System.out.println("Los " + euros + "� " + " son equivalentes a " + dolares + "$");
			break;
			
		// Creo el case yenes
		case "yenes":
			// Calculo el importe de euros en yenes
			double yenes = euros*129.852;
			
			// Muestro el resultado
			System.out.println("Los " + euros + "� " + "son equivalentes a " + yenes + " yenes");
			break;
		default:
			
			// En el default muestro esto
			System.out.println("No has introducido ninguna moneda");
			break;
		}
	}

}
