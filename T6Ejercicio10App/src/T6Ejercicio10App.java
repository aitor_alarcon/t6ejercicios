import java.util.Scanner;

public class T6Ejercicio10App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Llamo a la funci�n rellenarArray
		rellenarArray();
	}
	
	// Creo la funcion rellenarArray
	public static void rellenarArray() {
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Le pido el tama�o del array
		System.out.println("Que tama�o quieres que tenga el array?");
		int tama�oArray = teclado.nextInt();
		
		// Le pido el rango de n�meros para generar los aleatorios
		System.out.println("Entre que n�meros quieres que se generen los n�meros aleatorios");
		System.out.println("Introduce el primer n�mero");
		int num1 = teclado.nextInt();
		System.out.println("Introduce el segundo n�mero (Mayor que el primero)");
		int num2 = teclado.nextInt();
		
		// Creo el array
		int array[] = new int[tama�oArray];
		
		// Relleno el array con n�meros random y primos
		for (int i = 0; i < array.length; i++) {
			
			int numAleatorio = (int) Math.floor(Math.random()*(num1-num2+1)+num2);
			if(numPrimo(numAleatorio) == true) {
				array[i] = numAleatorio;
			}
			else {
				// Si el n�mero random no es primo hago que lo rellene con un 1
				array[i] = 1;
			}
			
		}
		
		// Llamo a la funci�n numMayor para mostrar el n�mero m�s grande del array
		System.out.println("El n�mero m�s grande del array es = " + numMayor(array));
	}
	
	// Creo la funcion numPrimo
	public static boolean numPrimo (int num) {
		
		// Creo un contador iniciado a 0
		int cont = 0;
		
		// Creo un bucle for que recorra el numero de veces igual 
		// al n�mero introducido
		for (int i = 1; i <= num; i++) {
			// Si el n�mero introducido entre el contador i 
			// es igual a 0 incremento el contador en 1
			if((num % i) == 0) {
				// Incremento el contador
				cont++;
			}
		}
		
		// si el contador es menor o igual a 2
		if(cont <= 2) {
			// Retorna true
			return true;
		}
		// si el contador es mayor que 2
		else {
			// Retorna false
			return false;
		}
	}
	
	// Creo la funci�n numMayor
	public static int numMayor(int array[]) {
		
		// Defino el numMayor igual al primero del array
		int numMayor = array[0];
		
		// Creo un for que recorra todo el array
		for (int i = 0; i < array.length; i++) {
			
			// Comparo el espacio del array correspondiente con numMayor
			if(array[i] > numMayor) {
				// Si el espacio del array es mayor a numMayor autom�ticamente
				// numMayor pasa a ser igual que el espacio del array en ese momento
				numMayor = array[i];
			}
		}
		
		// retorno numMayor
		return numMayor;
	}

}
